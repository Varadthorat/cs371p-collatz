// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// get length
// ----

TEST(CollatzFixture, updateCache0) {
    collatz_update_cache(186, 654321);
    ASSERT_EQ(cache[736112], 181);
}

TEST(CollatzFixture, updateCache1) {
    collatz_update_cache(8, 3);
    ASSERT_EQ(cache[16], 5);
}

TEST(CollatzFixture, updateCache2) {
    collatz_update_cache(62, 123456);
    ASSERT_EQ(cache[1929], 56);
}

TEST(CollatzFixture, updateCache3) {
    collatz_update_cache(26, 100);
    ASSERT_EQ(cache[58], 20);
}

// ----
// get length
// ----

TEST(CollatzFixture, getLength0) {
    ASSERT_EQ(collatz_get_length(999999), 259);
}

TEST(CollatzFixture, getLength1) {
    ASSERT_EQ(collatz_get_length(125), 109);
}

TEST(CollatzFixture, getLength2) {
    ASSERT_EQ(collatz_get_length(567), 62);
}

TEST(CollatzFixture, getLength3) {
    ASSERT_EQ(collatz_get_length(395832), 175);
}

TEST(CollatzFixture, getLength4) {
    ASSERT_EQ(collatz_get_length(124234), 150);
}

TEST(CollatzFixture, getLength5) {
    ASSERT_EQ(collatz_get_length(159487), 184);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(999999,3000)), make_tuple(999999, 3000, 525));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(23453,325321)), make_tuple(23453, 325321, 443));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(599332, 123)), make_tuple(599332, 123, 470));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(3333, 3333)), make_tuple(3333, 3333, 31));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
