// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;
extern int cache[];
// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ------------
// collatz_update_cache
// ------------

/**
 * update the intermediate values that are in the cycle of the long long value passed in
 * @param cycleLength the length of the cycle of the second paramter
 * @param currentNum the number that the cycle will start from for caching intermediate values
 */
void collatz_update_cache(int cycleLength, long long currentNum);

// ------------
// collatz_get_length
// ------------

/**
 * calculate the cycle length of the int passed in
 * @param num the number which the cycle length will be calculated for
 * @return the length of the cycle of the parameter
 */
int collatz_get_length (int num);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
