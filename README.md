# CS371p: Object-Oriented Programming Collatz Repo

* Name: (your Full Name)
  Varad Thorat
* EID: (your EID) 
  vvt255
* GitLab ID: (your GitLab ID)
  Varadthorat
* HackerRank ID: (your HackerRank ID)
  varadth
* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
  3fa7a83b69bc6f117141ee66584efe4b44bc04ae
* GitLab Pipelines: (link to your GitLab CI Pipeline)
  https://gitlab.com/Varadthorat/cs371p-collatz/-/pipelines
* Estimated completion time: (estimated time in hours, int or float)
  5 hours
* Actual completion time: (actual time in hours, int or float)
  9 hours
* Comments: (any additional comments you have)
  This project took a lot longer than expected. One of the things that I need to learn more about is utilizing
  the various Gitlab related matters(ex. pipelines and issues) because navigating it took a lot of time.