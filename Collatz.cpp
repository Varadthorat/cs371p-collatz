// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;
int cache[1000000];
// ------------
// collatz_read
// ------------
//
pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_update_cache
// ------------

void collatz_update_cache(int cycleLength, long long currentNum) {
    // we go through the cycle again, but this time we know the length of the entire
    //cycle, so we can cache intermediate values
    while(cycleLength != 1) {
        if(currentNum % 2 == 0) {
            currentNum/=2;
        } else {
            currentNum = currentNum * 3 + 1;
        }
        cycleLength--;
        //if the value is greater than 1000000, then it is greater than the cache size
        if(currentNum < 1000000) {
            cache[currentNum] = cycleLength;
        }
    }
}
// ------------
// collatz_get_length
// ------------

int collatz_get_length (int num) {
    assert(num > 0);
    // we keep the value stored as a long long because as we go through the cycle,
    // the values may overflow an integer
    long long currentNum = num;
    int length = 1;
    while(currentNum != 1) {
        if(currentNum % 2 == 0) {
            currentNum/=2;
        } else {
            currentNum = currentNum + (currentNum >> 1) + 1;
            length++;
        }
        length++;
    }
    //we cache the length of this num for easier retrieval later
    //we also cache the values of the intermediate values as well
    cache[num] = length;
    collatz_update_cache(length, num);
    assert(cache[num] > 0);
    return cache[num];
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    assert(i > 0);
    assert(j > 0);
    // <your code>
    //we check which one is greater to make it easier to loop through
    int lower = i < j ? i : j;
    int higher = i > j ? i : j;
    if((higher/2 + 1) > lower) {
        lower = higher/2 + 1;
    }
    int maxCycleLength = 0;
    for(int num = lower; num <= higher; num++) {
        if(!cache[num]) {
            //if the element in cache at the num index is 0, then we have not calculated it yet
            int length = collatz_get_length(num);
            if(length > maxCycleLength) {
                maxCycleLength = length;
            }
        } else {
            //we have already cached this num earlier so we can just compare lengths
            if(cache[num] > maxCycleLength) {
                maxCycleLength = cache[num];
            }
        }
    }
    assert(maxCycleLength > 0);
    return make_tuple(i, j, maxCycleLength);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
